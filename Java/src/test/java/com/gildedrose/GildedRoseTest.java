package com.gildedrose;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GildedRoseTest {

    @Test
    void updateQuality_AgeBrie() {
        Item[] items = new Item[] { getItem("Aged Brie", 2, 0),
                getItem("Aged Brie", 1, 0),
                getItem("Aged Brie", 0, 0),
                getItem("Aged Brie", -1, 0),
                getItem("Aged Brie", 2, 49),
                getItem("Aged Brie", 2, 50),
                getItem("Aged Brie", 1, 49),
                getItem("Aged Brie", 0, 49),
                getItem("Aged Brie", 0, 48)};
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertions("Aged Brie", 1, 1,
                app.items[0].name, app.items[0].quality, app.items[0].sellIn);
        assertions("Aged Brie", 1, 0,
                app.items[1].name, app.items[1].quality, app.items[1].sellIn);
        assertions("Aged Brie", 2, -1,
                app.items[2].name, app.items[2].quality, app.items[2].sellIn);
        assertions("Aged Brie", 2, -2,
                app.items[3].name, app.items[3].quality, app.items[3].sellIn);
        assertions("Aged Brie", 50, 1,
                app.items[4].name, app.items[4].quality, app.items[4].sellIn);
        assertions("Aged Brie", 50, 1,
                app.items[5].name, app.items[5].quality, app.items[5].sellIn);
        assertions("Aged Brie", 50, 0,
                app.items[6].name, app.items[6].quality, app.items[6].sellIn);
        assertions("Aged Brie", 50, -1,
                app.items[7].name, app.items[7].quality, app.items[7].sellIn);
        assertions("Aged Brie", 50, -1,
                app.items[8].name, app.items[8].quality, app.items[8].sellIn);
    }

    @Test
    void updateQuality_BackstagePasses() {
        Item[] items = new Item[] { getItem("Backstage passes to a TAFKAL80ETC concert", 15, 20),
                getItem("Backstage passes to a TAFKAL80ETC concert", 11, 20),
                getItem("Backstage passes to a TAFKAL80ETC concert", 10, 20),
                getItem("Backstage passes to a TAFKAL80ETC concert", 6, 20),
                getItem("Backstage passes to a TAFKAL80ETC concert", 5, 20),
                getItem("Backstage passes to a TAFKAL80ETC concert", 1, 20),
                getItem("Backstage passes to a TAFKAL80ETC concert", 0, 20),
                getItem("Backstage passes to a TAFKAL80ETC concert", 0, 50),
                getItem("Backstage passes to a TAFKAL80ETC concert", -1, 49),
                getItem("Backstage passes to a TAFKAL80ETC concert", 5, 49),
                getItem("Backstage passes to a TAFKAL80ETC concert", 10, 49)};
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertions("Backstage passes to a TAFKAL80ETC concert", 21, 14,
                app.items[0].name, app.items[0].quality, app.items[0].sellIn);
        assertions("Backstage passes to a TAFKAL80ETC concert", 21, 10,
                app.items[1].name, app.items[1].quality, app.items[1].sellIn);
        assertions("Backstage passes to a TAFKAL80ETC concert", 22, 9,
                app.items[2].name, app.items[2].quality, app.items[2].sellIn);
        assertions("Backstage passes to a TAFKAL80ETC concert", 22, 5,
                app.items[3].name, app.items[3].quality, app.items[3].sellIn);
        assertions("Backstage passes to a TAFKAL80ETC concert", 23, 4,
                app.items[4].name, app.items[4].quality, app.items[4].sellIn);
        assertions("Backstage passes to a TAFKAL80ETC concert", 23, 0,
                app.items[5].name, app.items[5].quality, app.items[5].sellIn);
        assertions("Backstage passes to a TAFKAL80ETC concert", 0, -1,
                app.items[6].name, app.items[6].quality, app.items[6].sellIn);
        assertions("Backstage passes to a TAFKAL80ETC concert", 0, -1,
                app.items[7].name, app.items[7].quality, app.items[7].sellIn);
        assertions("Backstage passes to a TAFKAL80ETC concert", 0, -2,
                app.items[8].name, app.items[8].quality, app.items[8].sellIn);
        assertions("Backstage passes to a TAFKAL80ETC concert", 50, 4,
                app.items[9].name, app.items[9].quality, app.items[9].sellIn);
        assertions("Backstage passes to a TAFKAL80ETC concert", 50, 9,
                app.items[10].name, app.items[10].quality, app.items[10].sellIn);
    }

    @Test
    void updateQuality_Sulfuras() {
        Item[] items = new Item[] { getItem("Sulfuras, Hand of Ragnaros", 15, 80),
                getItem("Sulfuras, Hand of Ragnaros", 1, 80),
                getItem("Sulfuras, Hand of Ragnaros", 0, 80),
                getItem("Sulfuras, Hand of Ragnaros", -1, 80)};
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertions("Sulfuras, Hand of Ragnaros", 80, 15,
                app.items[0].name, app.items[0].quality, app.items[0].sellIn);
        assertions("Sulfuras, Hand of Ragnaros", 80, 1,
                app.items[1].name, app.items[1].quality, app.items[1].sellIn);
        assertions("Sulfuras, Hand of Ragnaros", 80, 0,
                app.items[2].name, app.items[2].quality, app.items[2].sellIn);
        assertions("Sulfuras, Hand of Ragnaros", 80, -1,
                app.items[3].name, app.items[3].quality, app.items[3].sellIn);
    }

    @Test
    void updateQuality_Normal() {
        Item[] items = new Item[] { getItem("Elixir of the Mongoose", 15, 20),
                getItem("Elixir of the Mongoose", 15, 0),
                getItem("Elixir of the Mongoose", 1, 20),
                getItem("Elixir of the Mongoose", 0, 20),
                getItem("Elixir of the Mongoose", -1, 20),
                getItem("Elixir of the Mongoose", 0, 1)};
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertions("Elixir of the Mongoose", 19, 14,
                app.items[0].name, app.items[0].quality, app.items[0].sellIn);
        assertions("Elixir of the Mongoose", 0, 14,
                app.items[1].name, app.items[1].quality, app.items[1].sellIn);
        assertions("Elixir of the Mongoose", 19, 0,
                app.items[2].name, app.items[2].quality, app.items[2].sellIn);
        assertions("Elixir of the Mongoose", 18, -1,
                app.items[3].name, app.items[3].quality, app.items[3].sellIn);
        assertions("Elixir of the Mongoose", 18, -2,
                app.items[4].name, app.items[4].quality, app.items[4].sellIn);
        assertions("Elixir of the Mongoose", 0, -1,
                app.items[5].name, app.items[5].quality, app.items[5].sellIn);
    }

    @Test
    void updateQuality_Conjured() {
        Item[] items = new Item[] { getItem("Conjured Mana Cake", 15, 20),
                getItem("Conjured Mana Cake", 15, 1),
                getItem("Conjured Mana Cake", 15, 2),
                getItem("Conjured Mana Cake", 1, 20),
                getItem("Conjured Mana Cake", 0, 20),
                getItem("Conjured Mana Cake", -1, 20),
                getItem("Conjured Mana Cake", 0, 2),
                getItem("Conjured Mana Cake", 0, 3),
                getItem("Conjured Mana Cake", -1, 4)};
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertions("Conjured Mana Cake", 18, 14,
                app.items[0].name, app.items[0].quality, app.items[0].sellIn);
        assertions("Conjured Mana Cake", 0, 14,
                app.items[1].name, app.items[1].quality, app.items[1].sellIn);
        assertions("Conjured Mana Cake", 0, 14,
                app.items[2].name, app.items[2].quality, app.items[2].sellIn);
        assertions("Conjured Mana Cake", 18, 0,
                app.items[3].name, app.items[3].quality, app.items[3].sellIn);
        assertions("Conjured Mana Cake", 16, -1,
                app.items[4].name, app.items[4].quality, app.items[4].sellIn);
        assertions("Conjured Mana Cake", 16, -2,
                app.items[5].name, app.items[5].quality, app.items[5].sellIn);
        assertions("Conjured Mana Cake", 0, -1,
                app.items[6].name, app.items[6].quality, app.items[6].sellIn);
        assertions("Conjured Mana Cake", 0, -1,
                app.items[7].name, app.items[7].quality, app.items[7].sellIn);
        assertions("Conjured Mana Cake", 0, -2,
                app.items[8].name, app.items[8].quality, app.items[8].sellIn);
    }

    private Item getItem(String name, int sellIn, int quality) {
        return new Item(name, sellIn, quality);
    }

    private void assertions(String expectedName, int expectedQuality, int expectedSellIn,
                            String name, int quality, int sellIn) {
        assertEquals(expectedName, name);
        assertEquals(expectedQuality, quality);
        assertEquals(expectedSellIn, sellIn);
    }
}
