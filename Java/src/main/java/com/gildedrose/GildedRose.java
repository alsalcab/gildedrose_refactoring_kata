package com.gildedrose;

class GildedRose {
    Item[] items;

    public GildedRose(Item[] items) {
        this.items = items;
    }

    public void updateQuality() {
        // For all the Items
        for (int i = 0; i < items.length; i++) {
            // Quality update
            items[i].quality = qualityUpdate(items[i].name, items[i].quality);
            // Quality update based on the sellIn Value
            items[i].quality = qualityUpdateBasedOnSellInValue(items[i]);
            // sellIn update
            items[i].sellIn = sellInUpdate(items[i].name, items[i].sellIn);
            // Quality Update
            items[i].quality = qualityUpdate(items[i]);
        }
    }

    private int qualityUpdate (Item item) {
        // "Sulfuras, Hand of Ragnaros" never decreases in Quality
        return !item.name.equals("Sulfuras, Hand of Ragnaros") ?
                qualityUpdateBasedOnSellInValue(item.name, item.quality, item.sellIn) : item.quality;
    }

    private int qualityUpdateBasedOnSellInValue (Item item) {
        // "Backstage passes" increases in Quality as its SellIn value approaches
        return item.name.equals("Backstage passes to a TAFKAL80ETC concert") ?
                qualityUpdateForBackstage(item.quality, item.sellIn) : item.quality;
    }

    private int qualityUpdate(String name, int quality) {
        // "Sulfuras, Hand of Ragnaros" never updates its quality
        return !name.equals("Sulfuras, Hand of Ragnaros") ? getNewQuality(name, quality) : quality;
    }

    private int sellInUpdate(String name, int sellIn) {
        // "Sulfuras", being a legendary item, never has to be sold
        return !name.equals("Sulfuras, Hand of Ragnaros") ? sellIn - 1 : sellIn;
    }

    private int qualityUpdateBasedOnSellInValue(String name, int quality, int sellIn) {
        return sellIn < 0 ? qualityUpdateBasedOnName(name, quality) : quality;
    }

    private int qualityUpdateBasedOnName(String name, int quality) {
        // Quality drops to 0 after the concert
        return name.equals("Backstage passes to a TAFKAL80ETC concert") ? 0 : getNewQuality(name, quality);
    }

    private int qualityUpdateForBackstage(int quality, int sellIn) {
        // Quality increases by 2 when there are 10 days or less (quality is never more than 50)
        quality = sellIn < 11 && quality < 50 ? quality + 1 : quality;
        // Quality increases by 3 when there are 5 days or less (quality is never more than 50)
        return sellIn < 6 && quality < 50 ? quality + 1 : quality;
    }

    private int getNewQuality(String name, int quality) {
        // "Backstage passes", like aged brie, increases in Quality as its SellIn value approaches
        // The rest of Items are constantly degrading in quality as they approach their sell by date
        quality = name.equals("Aged Brie") || name.equals("Backstage passes to a TAFKAL80ETC concert")
                ? increaseQuality(quality) : decreaseQuality(quality);

        // "Conjured" items degrade in Quality twice as fast as normal items
        return name.equals("Conjured Mana Cake") ? decreaseQuality(quality) : quality;
    }

    private int decreaseQuality(int quality) {
        // The Quality of an item is never negative
        return quality > 0 ? quality - 1 : quality;
     }

    private int increaseQuality(int quality) {
        // The Quality of an item is never more than 50
        return quality < 50 ? quality + 1 : quality;
    }
}