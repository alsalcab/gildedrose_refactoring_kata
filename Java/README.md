# Gilded Rose Refactoring Kata

Gilded Rose Refactoring Kata is a java application that updates the quality of a certain number of
items, based on the requirements. Currently, there are five kinds of items (Aged Brie, Sulfuras, Backstage passes,
Conjured and the regular one). Every one has different requirements.
The application consists of:

- The class Item.java which is the class representing every Item (I have not modified this class since it was 
  requested like that in the file GildedRoseRequirements.txt).
- The class GildedRose.java which contains the business of the Application. It updates the quality
  of the items. (I have not modified the items property since it was requested like that in the file
  GildedRoseRequirements.txt).
- The class Main.java which is the entry point of the Application.
- The class GildedRoseTest which contains the implementation of the unit tests of the class GildedRose.java.

## How to use this Application:

In order to use this application, you need to:
- Clone the git repository from bitbucket.org (you need access to it).
- Build the application.
- Execute the .jar generated.

##Steps to follow in order to build the application:

- Open a session in Command Prompt.
- Move to the directory "Java" of your local repository by the Command Prompt.
- Execute the command:  
                        ./gradlew clean build

##Steps to follow in order to run the application:

- Build the application following the steps of the last paragraph called "Steps to follow in order to build the application".
- Once the build is finished, the "build" directory will be created inside the "Java" directory. This build directory
  will contain the executable .jar that will allow us to run the application.
- Move to the directory "build/libs" of your local repository by the Command Prompt.
- Execute the command:  
                    java -jar gilded-rose-kata-0.0.1-SNAPSHOT.jar